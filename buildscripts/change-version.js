const fs = require('fs-extra');

const appname = process.argv.slice(2)[0];
const version = process.argv.slice(2)[1];
const bumpMinor = process.argv.slice(2)[2] ? process.argv.slice(2)[2] !== 'false' : true;
const versionJsonPath = `./release.json`;

fs.readJSON(versionJsonPath, (err, json) => {
	if(err) {
		console.log(err);
	} else {
		updateJsonFile(versionJsonPath, json);
	}
});

function updateJsonFile(filePath, json) {
    json[appname].version = version;
    json[appname].bumpMinor = bumpMinor;
	fs.writeFile(filePath, JSON.stringify(json, null, 4) + '\n', (err) => {
		console.log('Successfully changing version to ' + json[appname].version + ' in ' + filePath);
	});
}
