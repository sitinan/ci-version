const fs = require('fs-extra');

const repo = process.argv.slice(2)[0];
const changeType = process.argv.slice(2)[1];
const branch = process.argv.slice(2)[2];
const versionJsonPath = `./${repo}/version.json`;
const currentVersion = require(`../${repo}/version.json`).version[branch];


fs.readJSON(versionJsonPath, (err, json) => {
	if(err) {
		console.log(err);
	} else {
		updateJsonFile(versionJsonPath, json);
	}
});

function updateJsonFile(filePath, json) {
	json.version[branch] = bumpVersionByChangeType(currentVersion, changeType);
	fs.writeFile(filePath, JSON.stringify(json, null, 4) + '\n', (err) => {
		console.log('Successfully bumping version to ' + json.version[branch] + ' in branch '+ branch + ' in ' + filePath);
	});
}

function bumpVersionByChangeType(version, changeType) {
	let versionList = version.split('.');
	switch(changeType) {
		case 'major':
            versionList[1] = increaseVersion(versionList[1]);
            versionList[2] = 0;
			return versionList.join(".");
		case 'minor':
            versionList[2] = increaseVersion(versionList[2]);
			return versionList.join(".");
		default:
			return version;
	}
}

function increaseVersion(stringNum) {
	return  parseInt(stringNum, 10) + 1;
}
